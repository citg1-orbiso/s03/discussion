package com.zuitt;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class CalculatorServlet
 */
public class CalculatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException{
		System.out.println("********************");
		System.out.println("Initialized connection to database.");
		System.out.println("********************");
	}	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /*
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    /*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}*/

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
	        System.out.println("Hello from the calculator servlet.");
	    	int num1 = Integer.parseInt(req.getParameter("num1"));
	    	int num2 = Integer.parseInt(req.getParameter("num2"));
	    	int total = num1 + num2;
	    	PrintWriter out = res.getWriter();
	    	out.println("<h1>The total of the two numbers are " + total+ "</h1>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You have accessed the get method of the calculator servlet</h1>");
	}
	
	public void destroy() {
		System.out.println("********************");
		System.out.println("Disconnected from database.");
		System.out.println("********************");		
	}
}